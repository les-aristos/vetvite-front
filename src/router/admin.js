import Customers from '../views/Admin/Customer/Customers';
import Offices from '../views/Admin//Office/Offices';
import Pros from '../views/Admin//Pro/Pros';
import CareTypes from '../views/Admin/CareType/CareTypes';
import CustomerDetail from '../views/Admin/Customer/CustomerDetail';
import OfficeDetail from '../views/Admin/Office/OfficeDetail';
import ProDetail from '../views/Admin/Pro/ProDetail';
import CareTypeDetail from '../views/Admin/CareType/CareTypeDetail';

export default [
    {
        path: '/admin/customers',
        name: 'Customers',
        component: Customers,
        meta: {
            requiresAuth: true,
            requiresAdmin: true,
            hideFooter: true,
            hideHeader: true,
            showAdminDrawer: true
        }
    },
    {
        path: '/admin/customers/:customerId',
        name: 'CustomerDetail',
        component: CustomerDetail,
        meta: {
            requiresAuth: true,
            requiresAdmin: true,
            hideFooter: true,
            hideHeader: true,
            showAdminDrawer: true
        }
    },
    {
        path: '/admin/pros',
        name: 'Pros',
        component: Pros,
        meta: {
            requiresAuth: true,
            requiresAdmin: true,
            hideFooter: true,
            hideHeader: true,
            showAdminDrawer: true
        }
    },
    {
        path: '/admin/pros/:proId',
        name: 'ProDetail',
        component: ProDetail,
        meta: {
            requiresAuth: true,
            requiresAdmin: true,
            hideFooter: true,
            hideHeader: true,
            showAdminDrawer: true
        }
    },
    {
        path: '/admin/offices',
        name: 'Offices',
        component: Offices,
        meta: {
            requiresAuth: true,
            requiresAdmin: true,
            hideFooter: true,
            hideHeader: true,
            showAdminDrawer: true
        }
    },
    {
        path: '/admin/offices/:officeId',
        name: 'OfficeDetail',
        component: OfficeDetail,
        meta: {
            requiresAuth: true,
            requiresAdmin: true,
            hideFooter: true,
            hideHeader: true,
            showAdminDrawer: true
        }
    },
    {
        path: '/admin/caretypes',
        name: 'CareTypes',
        component: CareTypes,
        meta: {
            requiresAuth: true,
            requiresAdmin: true,
            hideFooter: true,
            hideHeader: true,
            showAdminDrawer: true
        }
    },
    {
        path: '/admin/caretypes/:careTypeId',
        name: 'CareTypeDetail',
        component: CareTypeDetail,
        meta: {
            requiresAuth: true,
            requiresAdmin: true,
            hideFooter: true,
            hideHeader: true,
            showAdminDrawer: true
        }
    },
];

