import Register from "../views/Authentication/Register";
import Login from "../views/Authentication/Login";
import ForgotPassword from "../views/Authentication/ForgotPassword";
import ConfirmationRegistration from "../views/Authentication/ConfirmationRegistration";
import ResetPassword from "@/views/Authentication/ResetPassword";

export default [
    {
        path: '/login',
        name: 'Login',
        component: Login,
        meta: {
            requiresAnonymous: true
        }
    },
    {
        path: '/register',
        name: 'Register',
        component: Register,
        meta: {
            requiresAnonymous: true
        }
    },
    {
        path: '/forgot-password',
        name: 'ForgotPassword',
        component: ForgotPassword,
        meta: {
            requiresAnonymous: true
        }
    },
    {
        path: '/confirm',
        name: 'ConfirmationRegistration',
        component: ConfirmationRegistration,
        meta: {
            requiresAuth: true,
        }
    },
    {
        path: '/reset',
        name: 'ResetPassword',
        component: ResetPassword,
        meta: {
            requiresAnonymous: true
        }
    }
];

