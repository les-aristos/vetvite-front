import Show from "@/views/Professional/Show";
import OfficeShow from "@/views/Professional/OfficeShow";
import Availabilities from "@/views/Professional/Availabilities";
import Offices from "@/views/Professional/Offices";
import Schedule from "@/views/Professional/Schedule";

export default [
    {
        path: '/professional/:id',
        name: 'Professional',
        component: Show,
        meta: {
            requiresAuth: true,
        }
    },
    {
        path: '/office/:officeId',
        name: 'Office',
        component: OfficeShow,
        meta: {
            requiresAuth: true,
        }
    },
    {
        path: '/profile/availabilities',
        name: 'MyAvailabilities',
        component: Availabilities,
        meta: {
            requiresAuth: true,
            requiresPro: true,
        }
    },
    {
        path: '/profile/offices',
        name: 'MyOffices',
        component: Offices,
        meta: {
            requiresAuth: true,
            requiresPro: true,
        }
    },
    {
        path: '/profile/schedule',
        name: 'MySchedule',
        component: Schedule,
        meta: {
            requiresAuth: true,
            requiresPro: true,
        }
    }
]

