import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import axios from './plugins/axios';
import utils from './plugins/utils';
import moment from 'moment'

Vue.prototype.moment = moment;
moment.locale('fr');

Vue.config.productionTip = false;

Vue.use(utils);
Vue.use(axios);

const token = localStorage.getItem('token');
if (token) {
    store.dispatch('loadCurrentUser', token)
        .then(() => {
            Vue.prototype.$apiRest.defaults.headers.common['Authorization'] = `Bearer ${token}`;
        })
        .catch(err => console.error(err));
}

new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
}).$mount('#app');
